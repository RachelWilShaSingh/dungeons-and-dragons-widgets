$( document ).ready( function() {

    // Player information

    stats = {
        "str" : 15,
        "dex" : 6,
        "con" : 13,
        "int" : 12,
        "wis" : 17,
        "cha" : 9
    };

    weapons = [
        {
            "name" : "Mace",
            "atk-bonus" : "",
            "damage-roll" : "1d6",
            "type" : "melee",
            "classification" : "simple-melee"
        },
        {
            "name" : "Light crossbow",
            "atk-bonus" : "",
            "damage-roll" : "1d8",
            "type" : "ranged",
            "classification" : "simple-ranged"
        }
    ];

    characterInfo = {
        "name" : "Terpomo Beleta",
        "class" : "cleric",
        "domain" : "life",
        "level" : 3,
        "race" : "human",
        "alignment" : "neutral-good",
        "background" : "Acolyte",
        "player-name" : "Rachel",
        "notes" : "God: Pelor (Grayhawk), god of sun and healing",
        "speed" : 0 // calculated by code based on race
    };

    // Proficiency in skills is determined by background and class
    // Cleric class:
    // Acolyte background:
    skills = [
        { "skill" : "acrobatics",       "ability-mod" : "dex" },
        { "skill" : "animal-handling",  "ability-mod" : "wis" },
        { "skill" : "arcana",           "ability-mod" : "int" },
        { "skill" : "athletics",        "ability-mod" : "str" },
        { "skill" : "deception",        "ability-mod" : "cha" },
        { "skill" : "history",          "ability-mod" : "int" },
        { "skill" : "insight",          "ability-mod" : "wis" },
        { "skill" : "intimidation",     "ability-mod" : "cha" },
        { "skill" : "investigation",    "ability-mod" : "int" },
        { "skill" : "medicine",         "ability-mod" : "wis" },
        { "skill" : "nature",           "ability-mod" : "int" },
        { "skill" : "perception",       "ability-mod" : "wis" },
        { "skill" : "performance",      "ability-mod" : "cha" },
        { "skill" : "persuasion",       "ability-mod" : "cha" },
        { "skill" : "religion",         "ability-mod" : "int" },
        { "skill" : "sleight-of-hand",  "ability-mod" : "dex" },
        { "skill" : "stealth",          "ability-mod" : "dex" },
        { "skill" : "survival",         "ability-mod" : "wis" }
    ];

    // Cleric, pg 57
    // Skill proficiency: Choose two from History, Insight, Medicine, Persuasion, and Religion
    classProficiencies = {
        "armor" : [ "light-armor", "medium-armor", "shields" ],
        "weapons" : [ "simple-melee", "simple-ranged" ],
        "tools": [],
        "saving-throws" : [ "wis", "cha" ],
        "skills" : [ "history", "medicine" ]
    };

    // Acolyte, pg 127
    // Languages: two of your choice
    backgroundProficiencies = {
        "skills" : [ "insight", "religion" ],
        "languages" : [ "sylvan", "draconic"]
    };

    // Human, pg 31
    // Languages: common, and one extra language.
    // Variant human traits: Two different ability scores of your choice increase by 1.
    //                       Gain proficiency in one skill of choice.
    //                       Gain one feat of your cohice.
    raceProficiencies = {
        "languages" : [ "common", "orc" ]
    };
    
    aggregatedProficiencies = []
    
    
    statTypes = [ "str", "dex", "con", "int", "wis", "cha" ];
    // Page initialization
    
    function InitializePage() {
        write_characterInfo();
        write_characterStats();
        write_characterStatMods();
    }
    InitializePage();
    
    // ----------------------------------------------------------------- 
    // ----------------------------------------------------------------- Updating fields
    // ----------------------------------------------------------------- 
    function write_characterInfo() {
        $( "#character-name" ).val( characterInfo["name"] );
        $( "#character-class" ).val( characterInfo["class"] + " (" + characterInfo["domain"] + ")" );
        $( "#character-level" ).val( characterInfo["level"] );
        $( "#character-background" ).val( characterInfo["background"] );
        //$( "#player-name" ).val( characterInfo["player-name"] );
        $( "#character-race" ).val( characterInfo["race"] );
        $( "#character-alignment" ).val( characterInfo["alignment"] );
        //$( "#notes" ).val( characterInfo["notes"] );

        // Figure out speed
        var race = characterInfo["race"]
        if ( race == "dwarf" || race == "gnome" || race == "halfling" )
        {
            characterInfo["speed"] = 20; // 15 in medium or heavy armor, except dwarves
        }
        else if ( race == "human" || race == "elf" || race == "half-elf" || race == "half-orc" )
        {
            characterInfo["speed"] = 30; // 20 in medium or heavy armor
        }
        
        $( "#speed" ).val( characterInfo["speed"] );
    }

    function write_characterStats() {
        // statTypes = [ "str", "dex", "con", "int", "wis", "cha" ];
        for ( var i = 0; i < statTypes.length; i++ ) {
            var stat = get_stat( statTypes[i] );
            $( "#stat-" + statTypes[i] + "-base" ).val( stat );
        }
    }
    
    function write_characterStatMods() {
        // statTypes = [ "str", "dex", "con", "int", "wis", "cha" ];
        // Calculating the mod values:
        for ( var i = 0; i < statTypes.length; i++ ) {
            var statModifier = format_plusminus( get_modifier( statTypes[i] ) );
            $( "#stat-" + statTypes[i] + "-mod" ).val( statModifier );
        }
        
        //$( "#roll-initiative-reference" ).html( "(d20 " + formatWithPlusMinus( getAbilityModifier( "dex" ) ) + ")" );
    }
    
    
    // ----------------------------------------------------------------- 
    // ----------------------------------------------------------------- Accessing character information
    // ----------------------------------------------------------------- 
    function get_stat( statName ) {
        // Stat
        return parseInt( stats[ statName ] );
    }

    function get_modifier( statName ) {
        // Ability modifier
        return Math.floor( ( get_stat( statName ) - 10 ) / 2 );
    }
    
    // ----------------------------------------------------------------- 
    // ----------------------------------------------------------------- Formatting
    // ----------------------------------------------------------------- 
    function format_plusminus( value ) {
        if ( value > 0 ) { return "+" + value; }
        else { return value; }
    }
    
    // ----------------------------------------------------------------- 
    // ----------------------------------------------------------------- xyz
    // ----------------------------------------------------------------- 

    
    // Calculations


    function getInspiration() {
        return 0; // how to calculate?
    }

    function getProficiencyBonus() {
        // Table on pg 15 of PHB
        if      ( characterInfo["level"] <= 4 ) { return 2; }
        else if ( characterInfo["level"] <= 8 ) { return 3; }
        else if ( characterInfo["level"] <= 12 ) { return 4; }
        else if ( characterInfo["level"] <= 16 ) { return 5; }
        else if ( characterInfo["level"] <= 20 ) { return 6; }
    }

    function getPassiveWisdom() {
        return 0; // how to calculate?
    }

    function getSkillModifier( skillName ) {
        return 0; // how calculate?
    }

    function getArmorClass() {
        return 0;
    }

    function getInitiative() {
        return 0;
    }

    function getSpeed() {
        return 0;
    }
    
    function isProficientWithWeapon( weaponType ) {
        for ( var i = 0; i < classProficiencies["weapons"].length; i++ )
        {
            if ( classProficiencies["weapons"][i] == weaponType )
            {
                return true;
            }
        }
        return false;
    }
    
    function aggregateProficiencies() {
        //aggregatedProficiencies
        
        /*
         * classProficiencies = {
                "armor" : [ "light-armor", "medium-armor", "shields" ],
                "weapons" : [ "simple-melee", "simple-ranged" ],
                "tools": [],
                "saving-throws" : [ "wis", "cha" ],
                "skills" : [ "history", "medicine" ]
            };
            * 
            * backgroundProficiencies = {
                "skills" : [ "insight", "religion" ],
                "languages" : [ "sylvan", "draconic"]
            };
         * */
         
        // Proficiencies
        $.each( classProficiencies, function( key, profList ) {
            var table = "#proficiency-info-" + key;
            for ( var i = 0; i < profList.length; i++ )
            {                
                if ( !aggregatedProficiencies.includes( profList[i] ) )
                {
                    aggregatedProficiencies.push( profList[i] );
                }
            }
        } );
        
        $.each( backgroundProficiencies, function( key, profList ) {
            var table = "#proficiency-info-" + key;
            for ( var i = 0; i < profList.length; i++ )
            {                
                if ( !aggregatedProficiencies.includes( profList[i] ) )
                {
                    aggregatedProficiencies.push( profList[i] );
                }
            }
        } );
        
        $.each( raceProficiencies, function( key, profList ) {
            var table = "#proficiency-info-" + key;
            for ( var i = 0; i < profList.length; i++ )
            {                
                if ( !aggregatedProficiencies.includes( profList[i] ) )
                {
                    aggregatedProficiencies.push( profList[i] );
                }
            }
        } );
        
        for ( var i = 0; i < aggregatedProficiencies.length; i++ )
        {
            var prof = aggregatedProficiencies[i]
            if ( statTypes.includes( prof ) )
            {
                // mark these
                $( "#" + prof + "-saving-throw-toggle" ).attr( "checked", "checked" );
            }
            
            $( "#proficiencies-info" ).append( "<li>" + prof + "</li>" );
        }
    }

    function calculateSkillModifiers() {
        // Skill modifiers
        for ( var i = 0; i < skills.length; i++ )
        {
            var skillName = skills[i]["skill"];

            var abilityMod = getAbilityModifier( skills[i]["ability-mod"] );
            var proficiencyBonus = getProficiencyBonus( skills[i]["ability-mod"] );
            var additionalMod = getSkillModifier( skills[i]["skill"] );

            var skillMod = abilityMod + proficiencyBonus;

            $( "#skill-" + skillName + "-mod" ).val( formatWithPlusMinus( skillMod ) );
        }
    }

    function calculateWeaponInfo() {
        for ( var i = 0; i < weapons.length; i++ )
        {
            // Compute effects for dex modifier and stuff
            var index = i+1;
            $( "#weapon-" + index + "-name" ).val( weapons[i]["name"] );
            $( "#weapon-" + index + "-atk-bonus" ).val( weapons[i]["atk-bonus"] );
            $( "#weapon-" + index + "-damage" ).val( weapons[i]["damage-roll"] );
            
            if ( weapons[i]["type"] == "melee" )
            {
                var proficient = isProficientWithWeapon( weapons[i]["classification"] );
                var proficiencyBonus = ( proficient ) ? getProficiencyBonus() : 0;
                
                // roll-attack-melee-reference
                $( "#roll-attack-melee-reference" ).html( "(d20 " 
                    + formatWithPlusMinus( getAbilityModifier( "str" ) ) 
                    + formatWithPlusMinus( proficiencyBonus ) 
                    + ")" );
                $( "#attack-roll-melee-weapon" ).html( "(" + weapons[i]["name"] + ")" );
            }
            else if ( weapons[i]["type"] == "ranged" )
            {
                // roll-attack-ranged-reference
                $( "#roll-attack-ranged-reference" ).html( "(d20 " + formatWithPlusMinus( getAbilityModifier( "dex" ) ) + ")"  );
                $( "#attack-roll-ranged-weapon" ).html( "(" + weapons[i]["name"] + ")" );
            }
        }
        
        // Armor
        // Humans in medium or heavy armor: speed goes from 30 to 20. Gnomes and halflings: speed goes from 20 to 15.
    }

    function calculateInspiration() {
        // Inspiration
        $( "#inspiration" ).val( getInspiration() );
    }

    function calculateProficiencyBonus() {
        // Proficiency bonus
        $( "#proficiency-bonus" ).val( formatWithPlusMinus( getProficiencyBonus() ) );
    }

    function calculatePassiveWisdom() {
        // Passive wisdom
        $( "#passive-wisdom" ).val( getPassiveWisdom() );
    }

    function calculateSavingThrows() {
        // Saving throws
        //
        // https://www.reddit.com/r/DungeonsAndDragons/comments/82u8ux/question_how_do_i_determine_my_saving_throws/
        for ( var i = 0; i < statTypes.length; i++ )
        {
            var statModifier = getAbilityModifier( statTypes[i] );
            var proficiencyModifier = 0;
        }
    }

    function init() {
        updateCharacterInfo();
        aggregateProficiencies();
        calculateStatModifiers();
        calculateSkillModifiers();
        calculateWeaponInfo();
        calculateInspiration();
        calculateProficiencyBonus();
        calculatePassiveWisdom();
        calculateSavingThrows();
    }

    // Returns a random integer between min (inclusive) and max (inclusive).
    function getRandom( min, max ) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    
    function rollDie( dtype ) {
        if ( dtype.toString().includes( "d" ) )
        {
            dtype = parseInt( dtype.replace( "d", "" ) );
        }
        return getRandom( 1, dtype );
    }

    $( ".roll-die" ).click( function() {
        var dieType = $( this ).attr( "id" );

        var maxValue = parseInt( dieType.replace( "roll-d", "" ) );
        var random = rollDie( maxValue );

        $( "#roll-result" ).val( random );
        $( "#last-roll" ).html( "Last roll: d" + maxValue );
    } );
    
    $( "#roll-initiative" ).click( function() {
        // d20 roll plus dex mod
        var abilityMod = getAbilityModifier( "dex" );
        var random = rollDie( "d20" ) + abilityMod;
        
        $( "#roll-result" ).val( random );
        $( "#last-roll" ).html( "Last roll: initiative (d20 " + formatWithPlusMinus( abilityMod ) + ")" );
    } );
    
    $( "#roll-attack-melee" ).click( function() {
    } );
    
    $( "#roll-attack-ranged" ).click( function() {
    } );

} );
