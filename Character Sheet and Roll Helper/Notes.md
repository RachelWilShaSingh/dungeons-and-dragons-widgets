# Terpomo Beleta

**Stats**

|                   | Strength | Dexterity | Constitution | Intelligence | Wisdom | Charisma |
| ----------------- | -------- | --------- | ------------ | ------------ | ------ | -------- |
| Base              | 15       | 6         | 13           | 12           | 17     | 9        |
| Ability Modifier  | +2       | -2        | +1           | +1           | +3     | -1       |

* Armor class: 15
* Initiative: -2
* Speed: 30
* Passive wisdom (perception): 9
* Inspiration: ?
* Proficiency bonus: 2
* Spellcasting ability stat for Cleric: Wisdom
* Spell attack modifier = your proficiency bonus + your wisdom modifier ( 2 + 3 )


**Skills**

| Skill             | Related Stat  | Modifier  | Proficiency   |
| ----------------- | ------------- | --------- | ------------- |
| Acrobatics        | dex           | -2        |               |
| Animal Handling   | wis           | +3        |               |
| Arcana            | int           | +1        |               |
| Athletics         | str           | +2        |               |
| Deception         | cha           | -1        |               |
| History           | int           | +3        | From class    |
| Insight           | wis           | +5        | From bg       |
| Intimidation      | cha           | -1        |               |
| Investigation     | int           | +1        |               |
| Medicine          | wis           | +5        | From class    |
| Nature            | int           | +1        |               |
| Perception        | wis           | +3        |               |
| Performance       | cha           | -1        |               |
| Persuasion        | cha           | -1        |               |
| Religion          | int           | +3        | From bg       |
| Sleight of Hand   | dex           | -2        |               |
| Stealth           | dex           | -2        |               |
| Survival          | wis           | +3        |               |

**Saving throws**

| Stat          | Modifier      | Proficiency       |
| ------------- | ------------- | ----------------- |
| Strength      | +2            |                   |
| Dexterity     | -2            |                   |
| Constitution  | +1            |                   |
| Intelligence  | +1            |                   |
| Wisdom        | +5            | x                 |
| Charisma      | +1            | ???               |

**Spells**

| Level | Spell                     | Casting time      | Range         | Components    | Duration                          | Info                                  | Terpomo               | Notes |
| ----- | ------------------------- | ----------------- | ------------- | ------------- | --------------------------------- | ------------------------------------- | --------------------- | ----- |
| 0     | Mending                   | 1 Minute          | Touch         | V, S, M       | Instantaneous                     | | | This spell repairs a single break or tear in an object you touch, such as broken chain link, two halves of a broken key, a torn cloack, or a leaking wineskin. As long as the break or tear is no larger than 1 foot in any dimension, you mend it, leaving no trace of the former damage. This spell can physically repair a magic item or construct, but the spell can’t restore magic to such an object. |
| 0     | Spare the Dying           | 1 Action          | Touch         | V, S          | Instantaneous                     | | | You touch a living creature that has 0 hit points. The creature becomes stable. This spell has no effect on undead or constructs. |
| 0     | Thaumaturgy               | 1 Action          | 30 feet       | V             | Up to 1 minute                    | | | Manifest a minor wonder
| 1     | Bless                     | 1 Action          | 30 feet       | V, S, M       | Concentration, up to 1 minute     | Add d4 to attack/saving throw         |                       | You bless up to three creatures of your choice within range. Whenever a target makes an attack roll or a saving throw before the spell ends, the target can roll a d4 and add the number rolled to the attack roll or saving throw. |
| 1     | Cure Wounds               | 1 Action          | Touch         | V, S          | Instantaneous                     | 1d8 + spellcasting ability modifier   | 1d8 + 5               | A creature you touch regains a number of hit points equal to 1d8 + your spellcasting ability modifier. This spell has no effect on undead or constructs. |
| 1     | Guiding Bolt              | 1 Action          | 120 feet      | V, S          | 1 round                           | 4d6 damage                            | | A flash of light streaks toward a creature of your choice within range. Make a ranged spell attack against the target. On a hit, the target takes 4d6 radiant damage, and the next attack roll made against this target before the end of your next turn has advantage, thanks to the mystical dim light glittering on the target until then. |
| 1     | Healing Word              | 1 Bonus Action    | 60 feet       | V             | Instantaneous                     | 1d4 + spellcasting ability modifier   | 1d4 + 5               | A creature of your choice that you can see within range regains hit points equal to 1d4 + your spellcasting ability modifier. This spell has no effect on undead or constructs. |
| 1     | Sanctuary                 | 1 Bonus Action    | 30 feet       | V, S, M       | 1 minute                          | Wisdom saving throw                   | | You ward a creature within range against attack until the spell ends, any creature who targets the warded creature with an attack or a harmful spell must first make a Wisdom saving throw. On a failed save, the creature must choose a new target or lose the attack or spell. This spell doesn’t protect the warded creature from area effects, such as the explosion of a fireball. If the warded creature makes an attack or casts a spell that affects an enemy creature, this spell ends. |
| 1     | Shield of Faith           | 1 Bonus Action    | 60 feet       | V, S, M       | Concentraiton, up to 10 minutes   | +2 bonus AC                           | | A shimmering field appears and surrounds a creature of your choice within range, granting it a +2 bonus to AC for the duration. |
| 2     | Calm Emotions             | 1 Action          | 60 feet       | V, S          | Concentration, up to 1 minute     | | | You attempt to suppress strong emotions in a group of people. Each humanoid in a 20-foot-radius sphere centered on a point you choose within range must make a Charisma saving throw; a creature can choose to fail this saving throw if it wishes. If a creature fails its saving throw, choose one of the following two effects. You can suppress any effect causing a target to be charmed or frightened. When this spell ends, any suppressed effect resumes, provided that its duration has not expired in the meantime. |
| 2     | Prayer of Healing         | 10 Minutes        | 30 feet       | V             | Instantaneous                     | 2d8 + spellcasting ability modifier   | 2d8 + 5               | Up to six creatures of your choice that you can see within range each regain hit points equal to 2d8 + your spellcasting ability modifier. This spell has no effect on undead or constructs. |
| 2     | Protection from Poison    | 1 Action          | Touch         | V, S          | 1 hour                            | | | You touch a creature. If it is poisoned, you neutralize the poison. If more than one poison afflicts the target, you neutralize one poison that you know is present, or you neutralize one at random. For the duration, the target has advantage on saving throws against being poisoned, and it has resistance to poison damage. |

## Roll info

| Roll type             | Formula                       | Terpomo       |
| --------------------- | ----------------------------- | ------------- |
| Roll initiative       | d20 + dex modifier + other    | d20 - 2       |
| Roll to hit           | d20 + attack bonus            | Mace: d20 + 4 |
| Melee attack          | d6 + str modifier             | d6 + 2        |
| Ranged attack         | d8 + dex modifier             | d8 - 2        |
| Ability check         | d20 + ability modifier        | |
| Skill check           | d20 + skill modifier          | |
| Saving throw          | d20 + saving throw modifier   | |
| Advantage             | Roll two d20, use higher      | |
| Disadvantage          | Roll two d20, use lower       | |
